# -*- coding: utf-8 -*-

import unittest
from DataConverter import DataConverter
from DataParser import DataParser

class TestDataConverter(unittest.TestCase):
    
    def test_init_ok_if_is_instance(self):
        """
        Test if DataConverter initialization is successful when given a DataParser.
        """
        path = "Test/Data/AllFiles"
        dataParser = DataParser(path)
        dataConverter = DataConverter(dataParser)
        self.assertEqual(dataConverter.parsedData, dataParser)
        self.assertEqual(dataConverter.convertedData, [])
        
    def test_init_fail_if_is_not_instance(self):
        """
        Test if DataConverter initialization raises a TypeError exception when 
        given a wrong type
        """
        wrong_type = "Fail"
        self.assertRaises(TypeError, DataConverter, wrong_type)
        
    def test_should_get_module_presentation_length(self):
        """
        Test if getModulePresentationLength return the correct number.
        """
        path = "Test/Data/AllFiles"
        final_result=["AAA","2013J","11391","Pass"]
        expected_result = '268' # Result not converted to int on the spot. Done later.
        
        dataParser = DataParser(path)
        dataParser.parse() # Important, can't get data otherwise
        dataConverter = DataConverter(dataParser)
        module_presentation_length = dataConverter.getModulePresentationLength(final_result)
        self.assertEqual(module_presentation_length, expected_result)
        
    def test_should_get_assessments(self):
        """
        Test if getAssessments return the correct list of list of str.
        """
        path = "Test/Data/AllFiles"
        final_result=["AAA","2013J","11391","Pass"]
        expected_result = [["1752","TMA"],["1753","TMA"],["1754","TMA"],
                           ["1755","TMA"],["1756","TMA"]] # id_assessment "1757" exist but no student has a score on it, so it isn't expected.
        expected_sublist_length = 2
        
        dataParser = DataParser(path)
        dataParser.parse() # Important, can't get data otherwise
        dataConverter = DataConverter(dataParser)
        assessments = dataConverter.getAssessments(final_result)
        self.assertEqual(len(assessments[0]),expected_sublist_length)
        self.assertListEqual(assessments, expected_result)
        
    def test_should_get_score(self):
        """
        Test if getScore return the correct number
        """
        path = "Test/Data/AllFiles"
        id_assessment = "1752"
        id_student = "11391"
        expected_result = "78" # Result not converted to int on the spot. Done later.
        
        dataParser = DataParser(path)
        dataParser.parse() # Important, can't get data otherwise
        dataConverter = DataConverter(dataParser)
        score = dataConverter.getScore(id_assessment, id_student)
        self.assertEqual(score, expected_result)
        
    def test_should_get_mean_eval(self):
        """
        Test if getMeanEval return the correct number
        """
        path = "Test/Data/AllFiles"
        id_assessment = "1752"
        expected_result = 70.11142061281338
        
        dataParser = DataParser(path)
        dataParser.parse() # Important, can't get data otherwise
        dataConverter = DataConverter(dataParser)
        mean_eval = dataConverter.getMeanEval(id_assessment)
        self.assertEqual(mean_eval, expected_result)
        
    def test_should_add_scores_and_mean_eval_when_len_2(self):
        """
        Test if addScoresAndMeanEval add correctly the scores and the mean_eval 
        to the assessments list when the assessments sublist length is 2
        """
        path = "Test/Data/AllFiles"
        final_result=["AAA","2013J","11391","Pass"]
        assessments = [["1752","TMA"],["1753","TMA"],["1754","TMA"],
                       ["1755","TMA"],["1756","TMA"]] # id_assessment "1757" exist but no student has a score on it, so it isn't expected.
        expected_result = [["1752","TMA","78",70.11142061281338],["1753","TMA","85",66.80116959064327],
                            ["1754","TMA","80",70.22658610271904],["1755","TMA","85",70.56765676567657],
                            ["1756","TMA","82",69.12751677852349]]
        
        dataParser = DataParser(path)
        dataParser.parse() # Important, can't get data otherwise
        dataConverter = DataConverter(dataParser)
        new_assessments = dataConverter.addScoresAndMeanEval(final_result, assessments)
        self.assertEqual(len(new_assessments[0]), 4)
        self.assertListEqual(new_assessments, expected_result)
        
    def test_should_raise_exception_when_assessments_len_not_2(self):
        """
        Test if addScoresAndMeanEval raises an exception whenn assessments contains
        sublist length different from 2
        """
        path = "Test/Data/AllFiles"
        final_result=["AAA","2013J","11391","Pass"]
        wrong_assessments = [["1752","TMA","78",70.11142061281338],["1753","TMA","85",66.80116959064327],
                            ["1754","TMA","80",70.22658610271904],["1755","TMA","85",70.56765676567657],
                            ["1756","TMA","82",69.12751677852349]]
        
        dataParser = DataParser(path)
        dataParser.parse() # Important, can't get data otherwise
        dataConverter = DataConverter(dataParser)
        self.assertRaises(ValueError, dataConverter.addScoresAndMeanEval, final_result, wrong_assessments)
        
    def test_should_get_mean_student_when_assessments_len_4(self):
        """
        Test if getMeanStudent return the correct mean of the student on 
        each evaluation from a presentation.
        """
        path = "Test/Data/AllFiles"
        assessments = [["1752","TMA","78",70.11142061281338],["1753","TMA","85",66.80116959064327],
                       ["1754","TMA","80",70.22658610271904],["1755","TMA","85",70.56765676567657],
                       ["1756","TMA","82",69.12751677852349]]
        expected_result = 82
        
        dataParser = DataParser(path)
        dataParser.parse() # Important, can't get data otherwise
        dataConverter = DataConverter(dataParser)
        mean_student = dataConverter.getMeanStudent(assessments)
        self.assertEqual(mean_student, expected_result)
        
    def test_should_raise_exception_when_assessments_len_4(self):
        """
        Test if getMeanStudent and getGlobalMean raises an exception whenn assessments contains
        sublist length different from 4
        """
        path = "Test/Data/AllFiles"
        wrong_assessments = [["1752","TMA"],["1753","TMA"],["1754","TMA"],
                             ["1755","TMA"],["1756","TMA"]]
        
        dataParser = DataParser(path)
        dataParser.parse() # Important, can't get data otherwise
        dataConverter = DataConverter(dataParser)
        self.assertRaises(ValueError, dataConverter.getMeanStudent, wrong_assessments)
        self.assertRaises(ValueError, dataConverter.getGlobalMean, wrong_assessments)
        
    def test_should_get_global_mean_when_assessments_len_4(self):
        """
        Test if getGlobalMean return the correct mean of each student on 
        each evaluation from a presentation.
        """
        path = "Test/Data/AllFiles"
        assessments = [["1752","TMA","78",70.11142061281338],["1753","TMA","85",66.80116959064327],
                       ["1754","TMA","80",70.22658610271904],["1755","TMA","85",70.56765676567657],
                       ["1756","TMA","82",69.12751677852349]]
        expected_result = 69.36686997007514
        
        dataParser = DataParser(path)
        dataParser.parse() # Important, can't get data otherwise
        dataConverter = DataConverter(dataParser)
        global_mean = dataConverter.getGlobalMean(assessments)
        self.assertEqual(global_mean, expected_result)
        
    def test_should_convert_code_module(self):
        """
        Test if code_module is converted correctly to the correct numerical value.
        """
        path = "Test/Data/AllFiles"
        code_module = "AAA"
        expected_result = 656565
        
        dataParser = DataParser(path)
        dataParser.parse() # Important, can't get data otherwise
        dataConverter = DataConverter(dataParser)
        converted_code_module = dataConverter.convertCodeModule(code_module)
        self.assertEqual(converted_code_module, expected_result)
        
    def test_should_convert_code_presentation(self):
        """
        Test if code_presentation is converted correctly to the correct numerical value.
        """
        path = "Test/Data/AllFiles"
        code_presentation = "2013J"
        expected_result = 201310
        
        dataParser = DataParser(path)
        dataParser.parse() # Important, can't get data otherwise
        dataConverter = DataConverter(dataParser)
        converted_code_presentation = dataConverter.convertCodePresentation(code_presentation)
        self.assertEqual(converted_code_presentation, expected_result)
        
    def test_should_convert_assessment_type(self):
        """
        Test if assessment_type is converted correctly to the correct numerical value.
        """
        path = "Test/Data/AllFiles"
        type_tma = "TMA"
        type_cma = "CMA"
        type_exam = "Exam"
        expected_result_tma = 1
        expected_result_cma = 2
        expected_result_exam = 3
        
        dataParser = DataParser(path)
        dataParser.parse() # Important, can't get data otherwise
        dataConverter = DataConverter(dataParser)
        converted_tma = dataConverter.convertAssessmentType(type_tma)
        converted_cma = dataConverter.convertAssessmentType(type_cma)
        converted_exam = dataConverter.convertAssessmentType(type_exam)
        self.assertEqual(converted_tma, expected_result_tma)
        self.assertEqual(converted_cma, expected_result_cma)
        self.assertEqual(converted_exam, expected_result_exam)
        
    def test_should_raise_exception_when_assessment_type_unknown(self):
        """
        Test if convertAssessmentType raise an exception when the assessment_type 
        is unknown
        """
        path = "Test/Data/AllFiles"
        wrong_type = "examen"
        
        dataParser = DataParser(path)
        dataParser.parse() # Important, can't get data otherwise
        dataConverter = DataConverter(dataParser)
        self.assertRaises(ValueError, dataConverter.convertAssessmentType, wrong_type)
        
    def test_should_convert_assessments(self):
        """
        Test if assessments is converted correctly to the correct numerical values.
        """
        path = "Test/Data/AllFiles"
        assessments = [["1752","TMA","78",70.11142061281338],["1753","TMA","85",66.80116959064327],
                       ["1754","TMA","80",70.22658610271904],["1755","TMA","85",70.56765676567657],
                       ["1756","TMA","82",69.12751677852349]]
        expected_result = [[1752,1,79.0,71.11142061281338],[1753,1,86.0,67.80116959064327],
                           [1754,1,81.0,71.22658610271904],[1755,1,86.0,71.56765676567657],
                           [1756,1,83.0,70.12751677852349]]
        
        dataParser = DataParser(path)
        dataParser.parse() # Important, can't get data otherwise
        dataConverter = DataConverter(dataParser)
        converted_assessments = dataConverter.convertAssessments(assessments)
        self.assertListEqual(converted_assessments, expected_result)
        
    def test_should_convert_label(self):
        """
        Test if the label is converted correctly to the right numerical value.
        """
        path = "Test/Data/AllFiles"
        label_pass = "Pass"
        label_distinction = "Distinction"
        label_fail = "Fail"
        label_withdrawn = "Withdrawn"
        expected_pass = 1
        expected_distinction = 1
        expected_fail = 0
        expected_withdrawn = 0
        
        dataParser = DataParser(path)
        dataParser.parse() # Important, can't get data otherwise
        dataConverter = DataConverter(dataParser)
        converted_pass = dataConverter.convertLabel(label_pass)
        converted_distinction = dataConverter.convertLabel(label_distinction)
        converted_fail = dataConverter.convertLabel(label_fail)
        converted_withdrawn = dataConverter.convertLabel(label_withdrawn)
        self.assertEqual(converted_pass, expected_pass)
        self.assertEqual(converted_distinction, expected_distinction)
        self.assertEqual(converted_fail, expected_fail)
        self.assertEqual(converted_withdrawn, expected_withdrawn)
        
    def test_should_raise_exception_when_label_unknown(self):
        """
        Test if convertLabel raise an exception when the label is unknown
        """
        path = "Test/Data/AllFiles"
        wrong_label = "FAILED"
        
        dataParser = DataParser(path)
        dataParser.parse() # Important, can't get data otherwise
        dataConverter = DataConverter(dataParser)
        self.assertRaises(ValueError, dataConverter.convertLabel, wrong_label)
        
    def test_should_save_converted_data(self):
        """
        Test if saveConvertedData store as a csv file the converted data.
        """
        path = "Test/Data/AllFiles"
        file_path = "Test/savedData.csv"
        converted_data = [[[656565, 201310, 11391, 268, 83.0, 70.36686997007514,
                           1752,1,79.0,71.11142061281338,1753,1,86.0,67.80116959064327,
                           1754,1,81.0,71.22658610271904,1755,1,86.0,71.56765676567657,
                           1756,1,83.0,70.12751677852349,0,0,0,0,0,0,0,0,0,0,0,0,
                           0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],1],
                          [[656565, 201310, 11391, 268, 83.0, 70.36686997007514,
                           1752,1,79.0,71.11142061281338,1753,1,86.0,67.80116959064327,
                           1754,1,81.0,71.22658610271904,1755,1,86.0,71.56765676567657,
                           1756,1,83.0,70.12751677852349,0,0,0,0,0,0,0,0,0,0,0,0,
                           0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],1]] # Testing two entries directly, to be sure multi entries are ok.
        expected_result = "656565;201310;11391;268;83.0;70.36686997007514;1752;1;79.0;71.11142061281338;1753;1;86.0;67.80116959064327;1754;1;81.0;71.22658610271904;1755;1;86.0;71.56765676567657;1756;1;83.0;70.12751677852349;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;1\n656565;201310;11391;268;83.0;70.36686997007514;1752;1;79.0;71.11142061281338;1753;1;86.0;67.80116959064327;1754;1;81.0;71.22658610271904;1755;1;86.0;71.56765676567657;1756;1;83.0;70.12751677852349;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;1\n"
        
        dataParser = DataParser(path) # No need to parse data here since it's not used.
        dataConverter = DataConverter(dataParser)
        dataConverter.convertedData = converted_data
        dataConverter.saveConvertedData(file_path)
        try:
            with open(file_path, "r") as file:
                file_content = file.read()
                print(file_content)
                self.assertEqual(file_content, expected_result)
        except IOError:
                raise IOError("Error : Could not open the file " + file_path)
                
        
    def test_should_load_converted_data(self):
        """
        Test if loadConvertedData load data from the csv file correctly
        """
        path = "Test/Data/AllFiles"
        file_path = "Test/dataToLoad.csv"
        expected_result = [[[656565, 201310, 11391, 268, 83.0, 70.36686997007514,
                           1752,1,79.0,71.11142061281338,1753,1,86.0,67.80116959064327,
                           1754,1,81.0,71.22658610271904,1755,1,86.0,71.56765676567657,
                           1756,1,83.0,70.12751677852349,0,0,0,0,0,0,0,0,0,0,0,0,
                           0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],1],
                          [[656565, 201310, 11391, 268, 83.0, 70.36686997007514,
                           1752,1,79.0,71.11142061281338,1753,1,86.0,67.80116959064327,
                           1754,1,81.0,71.22658610271904,1755,1,86.0,71.56765676567657,
                           1756,1,83.0,70.12751677852349,0,0,0,0,0,0,0,0,0,0,0,0,
                           0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],1]]
        
        dataParser = DataParser(path) # No need to parse data here since it's not used.
        dataConverter = DataConverter(dataParser)
        dataConverter.loadConvertedData(file_path)
        self.assertListEqual(dataConverter.convertedData, expected_result)
        
    
    def test_sould_convert_data(self):
        """
        Test if all the data are converted correctly to the right numerical values.
        """
        path = "Test/Data/AllFiles"
        expected_result = [[656565, 201310, 11391, 268, 83.0, 70.36686997007514,
                            1752,1,79.0,71.11142061281338,1753,1,86.0,67.80116959064327,
                            1754,1,81.0,71.22658610271904,1755,1,86.0,71.56765676567657,
                            1756,1,83.0,70.12751677852349,0,0,0,0,0,0,0,0,0,0,0,0,
                            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],1]
        
        dataParser = DataParser(path)
        dataParser.parse() # Important, can't get data otherwise
        dataConverter = DataConverter(dataParser)
        dataConverter.convertData()
        dataConverter.saveConvertedData("savedConvertedData.csv")
        self.assertListEqual(dataConverter.convertedData[0], expected_result)

    
if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestDataConverter)
    unittest.TextTestRunner(verbosity=0, buffer=True).run(suite)
