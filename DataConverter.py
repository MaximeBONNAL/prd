# -*- coding: utf-8 -*-

from DataParser import DataParser
from statistics import mean
from ast import literal_eval

class DataConverter():
    """
    Converter of the parsed data.
    
    Attributes
    ----------
    parsedData : DataParser
        The instance of the parsed data
    
    convertedData : List of list of float
        List of the converted numerical data.
        Follow the format [converted_code_module, converted_code_presentation,
                           converted_id_student, converted_module_presentation_length, 
                           converted_mean_student, converted_global_mean, 
                           converted_id_assessment_1, converted_type_assessment_1,
                           converted_score_1, converted_mean_eval_1, ...,
                           converted_id_assessment_14, converted_type_assessment_14,
                           converted_score_14, converted_mean_eval_14]
    """
    
    def __init__(self, parsedData):
        """Constructor of the DataConverter class.
    
        Constructor of the DataConverter class needing `parsedData`, an instance
        of a DataParser object.

        Parameters
        ----------
        parsedData : DataParser
            The instance of a DataParser object.
        convertedData : list of list of float
            The data converted to numerical values under the right format.

        Raises 
        ------
        TypeError
            if the parameter `parsedData` isn't an instance of DataParser.

        Returns
        -------
        None.
        """
        if isinstance(parsedData, DataParser):
            self.parsedData = parsedData
            self.convertedData = []
        else:
            raise TypeError("Wrong type for instance `parsedData`.")
        
    def convertData(self):
        """Convert parsed data to numerical data following a specific format.
        
        Data from students following presentations of modules are extracted
        and converted to numerical data. 
        
        Raises
        ------
        TypeError
            if the attribute `parsedData` isn't an instance of DataParser

        Returns
        -------
        None.

        """
        if isinstance(self.parsedData, DataParser):
            for final_result in self.parsedData.final_results:
                code_module, code_presentation, id_student = final_result[:3]
                module_presentation_length = self.getModulePresentationLength(final_result)
                assessments = self.getAssessments(final_result)
                if assessments != []:
                    assessments = self.addScoresAndMeanEval(final_result, assessments)
                    mean_student = self.getMeanStudent(assessments)
                    global_mean = self.getGlobalMean(assessments)
                    converted_mean_student = self.convertMeanAndScore(mean_student)
                    converted_global_mean = self.convertMeanAndScore(global_mean)
                    converted_assessments = self.convertAssessments(assessments)
                else:
                    converted_mean_student = 0
                    converted_global_mean = 0
                    converted_assessments = []
                converted_code_module = self.convertCodeModule(code_module)
                converted_code_presentation = self.convertCodePresentation(code_presentation)
                converted_label = self.convertLabel(final_result[3])
                converted_data = [[converted_code_module, converted_code_presentation,
                                           int(id_student), int(module_presentation_length), 
                                           converted_mean_student, converted_global_mean], converted_label]
                for index in range(14):
                    for indexj in range(4):
                        if converted_assessments != [] and index < len(converted_assessments):
                            converted_data[0].append(converted_assessments[index][indexj])
                        else:
                            converted_data[0].append(0)
                self.convertedData.append(converted_data)
        else:
            raise TypeError("Wrong type for instance `parsedData`.")
            
    def saveConvertedData(self, path):
        """Save the converted data to a file.
        
        Write in the file with `path` the csv equivalent of `convertedData` 

        Parameters
        ----------
        path : str
            The file to write into.

        Raises
        ------
        IOError
            If the file can't be open.

        Returns
        -------
        None.

        """
        try:
            with open(path, "w") as file:
                for data in self.convertedData:
                    csv_list = [ str(i) for i in data[0]]
                    csv_list.append(str(data[1]))
                    csv_string = ";".join(csv_list)
                    file.write(csv_string+"\n")
                file.close()
        except IOError:
            raise IOError("Error : Could not open the file " + path)
            
    def loadConvertedData(self, path):
        """Load the converted data from a file.
        
        Initialize `convertedData` from the file `path`
        

        Parameters
        ----------
        path : str
            The path where are stored the data.

        Raises
        ------
        IOError
            If the file can't be open.

        Returns
        -------
        None.

        """
        try:
            with open(path, "r") as file:
                converted_data = []
                for line in file:
                    list_line = line.strip().split(";")
                    data = [[literal_eval(i) for i in list_line[:-1]], literal_eval(list_line[-1])]
                    converted_data.append(data)
                self.convertedData = converted_data
        except IOError:
            raise IOError("Error : Could not open the file " + path)
                    
        
            
    
    def getModulePresentationLength(self, final_result):
        """Get the module presentation length of a presentation of a module 
        followed by a student.
        
        Get the module_presentation_length from a presentation of a module
        which are available in the `final_result` parameter.

        Parameters
        ----------
        final_result : List of str
            List containing the final score of a student to a presentation.
            Follow the format [code_module, code_presentation,
                               id_student, final_result]

        Returns
        -------
        int
            The module_presentation_length corresponding to the final_result 
            values.

        """
        return [item[2] for item in self.parsedData.modules_presentations_lengths
                if item[0] == final_result[0] and item[1] == final_result[1]][0]
    
    def getAssessments(self, final_result):
        """Get each assessment id and type of a presentation of a module.
        
        Get a list of assessment_type and id_assessment where code_module
        and code_presentation from `parsedData.assessments_types`
        match code_module and code_presentation from the `final_result` 
        parameter.
        

        Parameters
        ----------
        final_result : List of str
            List containing the final score of a student to a presentation.
            Follow the format [code_module, code_presentation,
                               id_student, final_result]

        Returns
        -------
        list of list of str
            List containing each id_assessment and assessment_type of
            a presentation of a module.
            Follow the format [[id_assessment, assessment_type]]

        """
        assessments = [[item[2], item[3]] for item in self.parsedData.assessments_types
                if item[0] == final_result[0] and item[1] == final_result[1]]
        # Check if the student has a score on the assessment. 
        # Otherwise, this assessment is deleted from the list to avoid bugs.
        index_to_pop = []
        an_assessment_taken = False
        for index, assessment in enumerate(assessments):
            assessment_taken = False
            for item in self.parsedData.scores:
                if assessment[0] == item[0] and final_result[2] == item[1]:
                    assessment_taken = True
                    an_assessment_taken = True
            if assessment_taken == False:
                index_to_pop.append(index)
        if an_assessment_taken == False:
            assessments = []
        else:
            if index_to_pop != []:
                if len(index_to_pop) > 1:
                    index_to_pop = index_to_pop[::-1]
                    for index in index_to_pop:
                        if len(assessments) <= 1:
                            assessments = []
                        else:
                            assessments.pop(index)
                else:
                    if len(assessments) <= 1:
                        assessments = []
                    else:
                        assessments.pop(index_to_pop[0])
                
        return assessments
    
    def getScore(self, id_assessment, id_student):
        """Get each score of a student to an assessment
        
        Get the score corresponding to the parameters `id_assessment`
        and `id_student`

        Parameters
        ----------
        id_assessment : str
            String corresponding to the id of the assessment.
        id_student : str
            String corresponding to the id of the student.

        Returns
        -------
        Int
            The score corresponding to the assessment taken by a student.

        """
        return [item[2] for item in self.parsedData.scores if item[0] == id_assessment and item[1] == id_student][0]
    
    def getMeanEval(self, id_assessment):
        """Get the mean of an assessment 
        
        Get the mean of each student having taken the assessment corresponding
        to the parameter `id_assessment`.
        
        Parameters
        ----------
        id_assessment : str
            String corresponding to the id of the assessment.

        Returns
        -------
        Float
            The mean of an assessment.

        """
        return mean([int(item[2]) for item in self.parsedData.scores
                    if item[0] == id_assessment])
    
    def addScoresAndMeanEval(self, final_result, assessments):
        """Add the score and the mean of each assessment of the parameter 
        `assessments` and adding them to their corresponding list.
        

        Parameters
        ----------
        final_result : List of str
            List containing the final score of a student to a presentation.
            Follow the format [code_module, code_presentation,
                               id_student, final_result].
        assessments : List of list of str
            List containing the id and type of each assessment taken by 
            a student.

        Raises
        ------
        ValueError
            If the length of a sublist of `assessments` is not 2.

        Returns
        -------
        assessments : List of list of str
            List containing the id and type of each assessment taken by 
            a student and his score and the mean of the assessment.

        """
        for index, assessment in enumerate(assessments):
            if len(assessment) == 2:
                score = self.getScore(assessment[0], final_result[2])
                meanEval = self.getMeanEval(assessment[0])
                assessments[index].append(score)
                assessments[index].append(meanEval)
            else:
                raise ValueError("Wrong length for list `assessments`.")
        return assessments
    
    def getMeanStudent(self, assessments):
        """Get the mean of a student to the assessments of a presentation.
        
        Get the mean of each score of a student available in the parameter
        `assessments`
        

        Parameters
        ----------
        assessments : List of list of str
            List containing the id and type of each assessment taken by 
            a student and his score and the mean of the assessment.

        Raises
        ------
        ValueError
            If the length of a sublist of `assessments` is not 4.

        Returns
        -------
        Float
            Mean of a student to a presentation.

        """
        if len(assessments[0]) == 4:
            return mean([int(item[2]) for item in assessments])
        else:
            raise ValueError("Wrong length for list `assessments`.")
    
    def getGlobalMean(self, assessments):
        """Get the mean of the mean of each assessment.
        
        Get the mean of each mean of the `assessments`
        

        Parameters
        ----------
        assessments : List of list of str
            List containing the id and type of each assessment taken by 
            a student and his score and the mean of the assessment.

        Raises
        ------
        ValueError
            If the length of a sublist of `assessments` is not 4.

        Returns
        -------
        Float
            Mean of mean to each assessment.

        """
        if len(assessments[0]) == 4:
            return mean([item[3] for item in assessments])
        else:
            raise ValueError("Wrong length for list `assessments`.")
            
    def convertCodeModule(self, code_module):
        """Convert the module code to a numerical value
        
        Convert the module code which follows the format "AAA" to a numerical 
        value following the format "000" where each caracter is converted 
        in it's numerical value in the ASCII table.
        Example : "AAA" becomes 656565
        

        Parameters
        ----------
        code_module : str
            Id of the module.

        Returns
        -------
        int
            Id of the module converted to a numerical value.

        """
        return int("".join([str(ord(char)) for char in code_module]))
    
    def convertCodePresentation(self, code_presentation):
        """Convert the presentation code to a numerical value
        
        Convert the presentation code which follows the format "0000A" to a
        numerical value following the format "000000" where the number value 
        is multiplied by 100 and the char is converted to it's numerical value
        in the ASCII table minus 64. For instance "2022A" become 202201 since
        A equivalent is 65 in the ASCII table
        

        Parameters
        ----------
        code_presentation : str
            Id of the presentation.

        Returns
        -------
        int
            Id of the presentation converted to a numerical value.

        """
        year = int(code_presentation[:4])
        month_num = ord(code_presentation[4]) - 64
        return year * 100 + month_num
    
    def convertMeanAndScore(self, score):
        """Add 1 to the mean or the score
        
        Add 1 to the mean or the score given in parameter to avoid a score
        of 0 in the neural network which wouldn't impact the neural network
        while it should.

        Parameters
        ----------
        score : Float
            A mean or a score.

        Returns
        -------
        Float
            Mean or score plus 1.

        """
        return float(score) + 1
    
    def convertAssessmentType(self, assessment_type):
        """Convert the assessment type to a numerical value
        
        Convert the assessment type to a numerical value. 
        "TMA" take the value 1.
        "CMA" take the value 2.
        "Exam" take the value 3.        

        Parameters
        ----------
        assessment_type : str
            The type of assessment.

        Raises
        ------
        ValueError
            If the value of assessment_type isn't "TMA", "CMA" or "Exam".

        Returns
        -------
        int
            Assessment type converted to a numerical value.

        """
        if assessment_type == "TMA":
            return 1
        elif assessment_type == "CMA":
            return 2
        elif assessment_type == "Exam":
            return 3
        else:
            raise ValueError("Unknown assessment_type.")
    
    def convertAssessments(self, assessments):
        """Convert each assessment value to numerical values.
        
        Convert assessment_type, score, and mean_eval to numerical value.
        

        Parameters
        ----------
        assessments : List of list of str
            List containing the id and type of each assessment taken by 
            a student and his score and the mean of the assessment.

        Returns
        -------
        assessments : List of list of float
            Assessments converted to a numerical value.

        """
        for index, assessment in enumerate(assessments):
            converted_assessment_type = self.convertAssessmentType(assessment[1])
            converted_score = self.convertMeanAndScore(assessment[2])
            converted_mean_eval = self.convertMeanAndScore(assessment[3])
            assessments[index] = [int(assessment[0]), converted_assessment_type,
                                  converted_score, converted_mean_eval]
        return assessments
    
    def convertLabel(self, label):
        """Convert the label to a numerical value.
        
        Convert the label to 0 when the student withdrawn or failed and 1 otherwise.
        

        Parameters
        ----------
        final_result : str
            The final result of the student to a presentation.

        Raises
        ------
        ValueError
            If the label is unknown.

        Returns
        -------
        int
            The label converted to a numerical value.

        """
        if label == "Pass" or label == "Distinction":
            return 1
        elif label == "Withdrawn" or label == "Fail":
            return 0
        else:
            raise ValueError("Unknown final_result")
    